//Import state hook from react
//import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import {Card, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}){
	//Checks to see if the data was successfully passed
	// console.log(props);
	// console.log(typeof props);

	const {_id, name, description, price} = courseProp;

	//Use state hook in this component to be able to store its state
	//States are used to keep track of information related to individual components
	/*
		Syntax:
		const [getter, setter] = useState(initialGetterValue);

		getter ito yung nanggagaling sa initial value
		setter ito yung magset ng increment value
	*/
	/*const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(30);
	const [isOpen, setIsOpen] = useState(false);
	*/
	// console.log(useState(0));

	//Function to keep track of the enrolles for a course
	/*function enroll(){
		//to increment the setter use a +1
		//setCount(count - 1);
		//if(countSeat > 0){
			setCount(count + 1);
			console.log('Enrollees: ' + count);
			setSeats(seats - 1);
			console.log('Seats:' + seats);*/
		/*}else{
			alert('Do not deduct to the seats.')
			alert('Do not add to the count.')	
		}*/
	//}

	//Define a useEffect hook to have the CourseCard component perform a certain task after every DOM update

	/*useEffect(() => {
		if(seats === 0){
			setIsOpen(true);
		}
	}, [seats])*/

	// Activity
	// const [count, setCount] = useState(0);
	// const [countSeat, setCountSeat] = useState(30);

	// function enroll(){
	// 	if(countSeat > 0){
	// 		setCount(count + 1);
	// 		console.log('Enrollees: ' + count);
	// 		setCountSeat(countSeat - 1);
	// 		console.log('Seats:' - count);
	// 	}else{
	// 		alert('Do not deduct to the seats.')
	// 		alert('Do not add to the count.')	
	// 	}
	// }

	
	return(
		<Card className = "mb-2">
			<Card.Body>
				<Card.Title>{name}</Card.Title>

				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>

				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>PHP {price}</Card.Text>

				<Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
			</Card.Body>
		</Card>
		
	)
}