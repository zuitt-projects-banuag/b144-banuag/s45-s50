//Mock Database
const courseData = [
	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi non lacus a odio tempor dictum. Suspendisse et orci elementum, congue sem id, pretium dui. Proin erat mi, malesuada nec eleifend et, mattis vel lacus.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Suspendisse quis ultricies magna. Nulla molestie nisl sed mi elementum, vel laoreet diam molestie. Nulla sed ultrices tellus.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Mauris vitae lacus sed ante pretium rhoncus at quis lacus. Proin consequat elementum mi, eget luctus diam hendrerit ut. Cras ornare sagittis ligula.",
		price: 55000,
		onOffer: true
	},
	{
		id: "wdc004",
		name: "HTML Introduction",
		description: "uspendisse nec pellentesque dolor. Nullam congue porta ipsum. Sed lobortis, libero tempus condimentum tincidunt, massa magna gravida eros, eu ullamcorper erat arcu non dui.",
		price: 60000,
		onOffer: true
	}
];

export default courseData;