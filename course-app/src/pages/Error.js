import Banner from '../components/Banner';

export default function Error(){

	//Render for Error
	const data = {
		title: "404 - Not Found",
		content: "The page you are looking for cannot be found",
		destination: "/",
		label: "Back Home"

	}

	return (
		//Reusable
		<Banner data={data}/>
	)
}

/*import { Fragment } from 'react';
import { ErrorBanner } from '../components/Banner';
//import Highlights from '../components/Highlights';
//import CourseCard from '../components/CourseCard';

export default function Error() {
	return(
		<Fragment>
			<ErrorBanner />
		</Fragment>
	)
}
*/
